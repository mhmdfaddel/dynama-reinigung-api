package dynama.reinigung.api.repositories;

import dynama.reinigung.api.models.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionsRepository extends JpaRepository<Permission, Long> {
}
