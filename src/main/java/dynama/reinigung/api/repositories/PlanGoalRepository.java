package dynama.reinigung.api.repositories;

import dynama.reinigung.api.models.PlanGoal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanGoalRepository extends JpaRepository<PlanGoal, Long> {
}
