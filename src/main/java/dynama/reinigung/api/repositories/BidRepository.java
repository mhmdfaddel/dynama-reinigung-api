package dynama.reinigung.api.repositories;

import dynama.reinigung.api.models.Bid;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BidRepository extends JpaRepository<Bid, Long> {
}
