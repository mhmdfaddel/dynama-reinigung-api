package dynama.reinigung.api.repositories;

import dynama.reinigung.api.models.User;
import dynama.reinigung.api.models.UserRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRateRepository extends JpaRepository<UserRate, Long> {
}
