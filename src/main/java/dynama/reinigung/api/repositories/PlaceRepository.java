package dynama.reinigung.api.repositories;

import dynama.reinigung.api.models.Place;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaceRepository extends JpaRepository<Place, Long> {
}
