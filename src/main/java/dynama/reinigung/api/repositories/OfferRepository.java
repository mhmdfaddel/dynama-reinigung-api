package dynama.reinigung.api.repositories;

import dynama.reinigung.api.models.Offer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OfferRepository extends JpaRepository<Offer, Long> {
}
