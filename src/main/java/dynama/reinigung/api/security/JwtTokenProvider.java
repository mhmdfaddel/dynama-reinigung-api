package dynama.reinigung.api.security;

import com.sun.security.auth.UserPrincipal;
import dynama.reinigung.api.models.Permission;
import dynama.reinigung.api.models.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.stream.Collectors;

@Component
public class JwtTokenProvider {

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.expirationMs}")
    private int jwtExpiration;

    public String generateToken(Authentication authentication) {
        User userPrincipal = (User) authentication.getPrincipal();
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpiration);

        Claims claims = Jwts.claims().setSubject(Long.toString(userPrincipal.getId()));
        claims.put("role", userPrincipal.getRole().getRoleName());
        claims.put("permissions", (userPrincipal.getRole()).getPermissions().stream().map(Permission::getName).collect(Collectors.toList()));

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public String generateToken(User user) {

        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpiration);

        Claims claims = Jwts.claims().setSubject(Long.toString(user.getId()));
        try {
            claims.put("role", user.getRole().getRoleName());
            claims.put("permissions", (user.getRole()).getPermissions().stream().map(Permission::getName).collect(Collectors.toList()));
        }
        catch(Exception ex){
            System.out.println(ex);
        }

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }
}
