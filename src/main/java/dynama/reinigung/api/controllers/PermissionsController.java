package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.PermissionNotFoundException;
import dynama.reinigung.api.models.Permission;
import dynama.reinigung.api.repositories.PermissionsRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class PermissionsController {

	private final PermissionsRepository repository;

	PermissionsController(PermissionsRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/permissions")
	List<Permission> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/permissions")
	Permission newPermission(@RequestBody Permission newPermission) {
		 return repository.save(newPermission);
	}

	// Single item

	@GetMapping("/permissions/{id}")
	Permission one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new PermissionNotFoundException(id));
	}

//	@PutMapping("/permissions/{id}")
//	Permission replacePermission(@RequestBody Permission newPermission, @PathVariable Long id) {
//
//		return repository.findById(id).map(permission -> {
//			permission.setName(newPermission.getName());
//			permission.setRole(newPermission.getRole());
//			return repository.save(permission);
//		}).orElseGet(() -> {
//			newPermission.setPermission_id(id);
//			return repository.save(newPermission);
//		});
//	}

	@DeleteMapping("/permissions/{id}")
	void deletePermission(@PathVariable Long id) {
		repository.deleteById(id);
	}
}