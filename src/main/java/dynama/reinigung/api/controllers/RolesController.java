package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.RoleNotFoundException;
import dynama.reinigung.api.models.Role;
import dynama.reinigung.api.repositories.RoleRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class RolesController {

	private final RoleRepository repository;

	RolesController(RoleRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/roles")
	List<Role> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/roles")
	Role newRole(@RequestBody Role newRole) {
		 return repository.save(newRole);
	}

	// Single item

	@GetMapping("/roles/{id}")
	Role one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new RoleNotFoundException(id));
	}

//	@PutMapping("/roles/{id}")
//	Role replaceRole(@RequestBody Role newRole, @PathVariable Long id) {
//
//		return repository.findById(id).map(role -> {
//			role.setName(newRole.getName());
//			role.setRole(newRole.getRole());
//			return repository.save(role);
//		}).orElseGet(() -> {
//			newRole.setRole_id(id);
//			return repository.save(newRole);
//		});
//	}

	@DeleteMapping("/roles/{id}")
	void deleteRole(@PathVariable Long id) {
		repository.deleteById(id);
	}
}