package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.BidNotFoundException;
import dynama.reinigung.api.models.Bid;
import dynama.reinigung.api.repositories.BidRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class BidsController {

	private final BidRepository repository;

	BidsController(BidRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/bids")
	List<Bid> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/bids")
	Bid newBid(@RequestBody Bid newBid) {
		 return repository.save(newBid);
	}

	// Single item

	@GetMapping("/bids/{id}")
	Bid one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new BidNotFoundException(id));
	}

//	@PutMapping("/bids/{id}")
//	Bid replaceBid(@RequestBody Bid newBid, @PathVariable Long id) {
//
//		return repository.findById(id).map(bid -> {
//			bid.setName(newBid.getName());
//			bid.setRole(newBid.getRole());
//			return repository.save(bid);
//		}).orElseGet(() -> {
//			newBid.setBid_id(id);
//			return repository.save(newBid);
//		});
//	}

	@DeleteMapping("/bids/{id}")
	void deleteBid(@PathVariable Long id) {
		repository.deleteById(id);
	}
}