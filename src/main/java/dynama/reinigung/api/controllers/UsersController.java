package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.UserNotFoundException;
import dynama.reinigung.api.models.User;
import dynama.reinigung.api.repositories.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api")
class UsersController {

	private final UserRepository repository;

	UsersController(UserRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/users")
	List<User> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/users")
	User newUser(@RequestBody User newUser) {
		 return repository.save(newUser);
	}

	// Single item

	@GetMapping("/users/{id}")
	User one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
	}


	@GetMapping("/user/{username}")
	public ResponseEntity<User> getUserByUsername(@PathVariable String username) {
		User user = repository.findByUsername(username);
		return ResponseEntity.ok(user);
	}

//	@PutMapping("/users/{id}")
//	User replaceUser(@RequestBody User newUser, @PathVariable Long id) {
//
//		return repository.findById(id).map(user -> {
//			user.setName(newUser.getName());
//			user.setRole(newUser.getRole());
//			return repository.save(user);
//		}).orElseGet(() -> {
//			newUser.setUser_id(id);
//			return repository.save(newUser);
//		});
//	}

	@DeleteMapping("/users/{id}")
	void deleteUser(@PathVariable Long id) {
		repository.deleteById(id);
	}
}