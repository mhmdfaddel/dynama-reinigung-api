package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.UserRateNotFoundException;
import dynama.reinigung.api.models.UserRate;
import dynama.reinigung.api.repositories.UserRateRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class UserRatesController {

	private final UserRateRepository repository;

	UserRatesController(UserRateRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/userRates")
	List<UserRate> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/userRates")
	UserRate newUserRate(@RequestBody UserRate newUserRate) {
		 return repository.save(newUserRate);
	}

	// Single item

	@GetMapping("/userRates/{id}")
	UserRate one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new UserRateNotFoundException(id));
	}

//	@PutMapping("/userRates/{id}")
//	UserRate replaceUserRate(@RequestBody UserRate newUserRate, @PathVariable Long id) {
//
//		return repository.findById(id).map(userRate -> {
//			userRate.setName(newUserRate.getName());
//			userRate.setRole(newUserRate.getRole());
//			return repository.save(userRate);
//		}).orElseGet(() -> {
//			newUserRate.setUserRate_id(id);
//			return repository.save(newUserRate);
//		});
//	}

	@DeleteMapping("/userRates/{id}")
	void deleteUserRate(@PathVariable Long id) {
		repository.deleteById(id);
	}
}