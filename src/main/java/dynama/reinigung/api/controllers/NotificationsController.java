package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.NotificationNotFoundException;
import dynama.reinigung.api.models.Notification;
import dynama.reinigung.api.repositories.NotificationRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class NotificationsController {

	private final NotificationRepository repository;

	NotificationsController(NotificationRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/notifications")
	List<Notification> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/notifications")
	Notification newNotification(@RequestBody Notification newNotification) {
		 return repository.save(newNotification);
	}

	// Single item

	@GetMapping("/notifications/{id}")
	Notification one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new NotificationNotFoundException(id));
	}

//	@PutMapping("/notifications/{id}")
//	Notification replaceNotification(@RequestBody Notification newNotification, @PathVariable Long id) {
//
//		return repository.findById(id).map(notification -> {
//			notification.setName(newNotification.getName());
//			notification.setRole(newNotification.getRole());
//			return repository.save(notification);
//		}).orElseGet(() -> {
//			newNotification.setNotification_id(id);
//			return repository.save(newNotification);
//		});
//	}

	@DeleteMapping("/notifications/{id}")
	void deleteNotification(@PathVariable Long id) {
		repository.deleteById(id);
	}
}