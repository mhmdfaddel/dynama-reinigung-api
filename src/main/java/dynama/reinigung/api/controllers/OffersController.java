package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.OfferNotFoundException;
import dynama.reinigung.api.models.Offer;
import dynama.reinigung.api.repositories.OfferRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class OffersController {

	private final OfferRepository repository;

	OffersController(OfferRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/offers")
	List<Offer> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/offers")
	Offer newOffer(@RequestBody Offer newOffer) {
		 return repository.save(newOffer);
	}

	// Single item

	@GetMapping("/offers/{id}")
	Offer one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new OfferNotFoundException(id));
	}

//	@PutMapping("/offers/{id}")
//	Offer replaceOffer(@RequestBody Offer newOffer, @PathVariable Long id) {
//
//		return repository.findById(id).map(offer -> {
//			offer.setName(newOffer.getName());
//			offer.setRole(newOffer.getRole());
//			return repository.save(offer);
//		}).orElseGet(() -> {
//			newOffer.setOffer_id(id);
//			return repository.save(newOffer);
//		});
//	}

	@DeleteMapping("/offers/{id}")
	void deleteOffer(@PathVariable Long id) {
		repository.deleteById(id);
	}
}