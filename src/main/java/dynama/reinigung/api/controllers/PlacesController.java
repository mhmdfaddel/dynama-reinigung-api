package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.PlaceNotFoundException;
import dynama.reinigung.api.models.Place;
import dynama.reinigung.api.repositories.PlaceRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class PlacesController {

	private final PlaceRepository repository;

	PlacesController(PlaceRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/places")
	List<Place> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/places")
	Place newPlace(@RequestBody Place newPlace) {
		 return repository.save(newPlace);
	}

	// Single item

	@GetMapping("/places/{id}")
	Place one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new PlaceNotFoundException(id));
	}

//	@PutMapping("/places/{id}")
//	Place replacePlace(@RequestBody Place newPlace, @PathVariable Long id) {
//
//		return repository.findById(id).map(place -> {
//			place.setName(newPlace.getName());
//			place.setRole(newPlace.getRole());
//			return repository.save(place);
//		}).orElseGet(() -> {
//			newPlace.setPlace_id(id);
//			return repository.save(newPlace);
//		});
//	}

	@DeleteMapping("/places/{id}")
	void deletePlace(@PathVariable Long id) {
		repository.deleteById(id);
	}
}