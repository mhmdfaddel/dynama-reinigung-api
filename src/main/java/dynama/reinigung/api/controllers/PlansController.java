package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.PlanNotFoundException;
import dynama.reinigung.api.models.Plan;
import dynama.reinigung.api.repositories.PlanRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class PlansController {

	private final PlanRepository repository;

	PlansController(PlanRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/plans")
	List<Plan> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/plans")
	Plan newPlan(@RequestBody Plan newPlan) {
		 return repository.save(newPlan);
	}

	// Single item

	@GetMapping("/plans/{id}")
	Plan one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new PlanNotFoundException(id));
	}

//	@PutMapping("/plans/{id}")
//	Plan replacePlan(@RequestBody Plan newPlan, @PathVariable Long id) {
//
//		return repository.findById(id).map(plan -> {
//			plan.setName(newPlan.getName());
//			plan.setRole(newPlan.getRole());
//			return repository.save(plan);
//		}).orElseGet(() -> {
//			newPlan.setPlan_id(id);
//			return repository.save(newPlan);
//		});
//	}

	@DeleteMapping("/plans/{id}")
	void deletePlan(@PathVariable Long id) {
		repository.deleteById(id);
	}
}