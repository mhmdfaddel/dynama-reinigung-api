package dynama.reinigung.api.controllers;

import dynama.reinigung.api.exceptions.PlanGoalNotFoundException;
import dynama.reinigung.api.models.PlanGoal;
import dynama.reinigung.api.repositories.PlanGoalRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class PlanGoalsController {

	private final PlanGoalRepository repository;

	PlanGoalsController(PlanGoalRepository repository) {
		this.repository = repository;
	}

	// Aggregate root
	// tag::get-aggregate-root[]
	@GetMapping("/planGoals")
	List<PlanGoal> all() {
		return repository.findAll();
	}
	// end::get-aggregate-root[]

	@PostMapping("/planGoals")
	PlanGoal newPlanGoal(@RequestBody PlanGoal newPlanGoal) {
		 return repository.save(newPlanGoal);
	}

	// Single item

	@GetMapping("/planGoals/{id}")
	PlanGoal one(@PathVariable Long id) {

		return repository.findById(id).orElseThrow(() -> new PlanGoalNotFoundException(id));
	}

//	@PutMapping("/planGoals/{id}")
//	PlanGoal replacePlanGoal(@RequestBody PlanGoal newPlanGoal, @PathVariable Long id) {
//
//		return repository.findById(id).map(planGoal -> {
//			planGoal.setName(newPlanGoal.getName());
//			planGoal.setRole(newPlanGoal.getRole());
//			return repository.save(planGoal);
//		}).orElseGet(() -> {
//			newPlanGoal.setPlanGoal_id(id);
//			return repository.save(newPlanGoal);
//		});
//	}

	@DeleteMapping("/planGoals/{id}")
	void deletePlanGoal(@PathVariable Long id) {
		repository.deleteById(id);
	}
}