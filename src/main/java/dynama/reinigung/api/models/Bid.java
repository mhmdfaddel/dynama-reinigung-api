package dynama.reinigung.api.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="bids")
public class Bid {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date valid_until;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    private User company;

    private String contract_type;

    private String total_area;

    private String area_unit;

    private float price_range_min;
    private float price_range_max;

    private Date created_at;

    private Date updated_at;

    private Date deleted_at;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getValid_until() {
        return valid_until;
    }

    public void setValid_until(Date valid_until) {
        this.valid_until = valid_until;
    }

    public User getCompany() {
        return company;
    }

    public void setCompany(User company) {
        this.company = company;
    }

    public String getContract_type() {
        return contract_type;
    }

    public void setContract_type(String contract_type) {
        this.contract_type = contract_type;
    }

    public String getTotal_area() {
        return total_area;
    }

    public void setTotal_area(String total_area) {
        this.total_area = total_area;
    }

    public String getArea_unit() {
        return area_unit;
    }

    public void setArea_unit(String area_unit) {
        this.area_unit = area_unit;
    }

    public float getPrice_range_min() {
        return price_range_min;
    }

    public void setPrice_range_min(float price_range_min) {
        this.price_range_min = price_range_min;
    }

    public float getPrice_range_max() {
        return price_range_max;
    }

    public void setPrice_range_max(float price_range_max) {
        this.price_range_max = price_range_max;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public Date getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Date updated_at) {
        this.updated_at = updated_at;
    }

    public Date getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Date deleted_at) {
        this.deleted_at = deleted_at;
    }
}
