package dynama.reinigung.api.exceptions;

public class NotificationNotFoundException extends RuntimeException {

    public NotificationNotFoundException(Long id) {
        super("Could not find Notification " + id);
    }
}