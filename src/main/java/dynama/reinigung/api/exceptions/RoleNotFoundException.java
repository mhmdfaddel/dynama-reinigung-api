package dynama.reinigung.api.exceptions;

public class RoleNotFoundException extends RuntimeException {

    public RoleNotFoundException(Long id) {
        super("Could not find Role " + id);
    }
}