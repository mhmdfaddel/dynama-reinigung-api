package dynama.reinigung.api.exceptions;

public class PermissionNotFoundException extends RuntimeException {

    public PermissionNotFoundException(Long id) {
        super("Could not find Permission " + id);
    }
}