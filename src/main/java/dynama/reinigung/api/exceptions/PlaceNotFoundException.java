package dynama.reinigung.api.exceptions;

public class PlaceNotFoundException extends RuntimeException {

    public PlaceNotFoundException(Long id) {
        super("Could not find Place " + id);
    }
}