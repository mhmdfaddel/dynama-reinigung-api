package dynama.reinigung.api.exceptions;

public class OfferNotFoundException extends RuntimeException {

    public OfferNotFoundException(Long id) {
        super("Could not find Offer " + id);
    }
}