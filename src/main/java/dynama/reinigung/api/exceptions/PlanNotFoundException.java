package dynama.reinigung.api.exceptions;

public class PlanNotFoundException extends RuntimeException {

    public PlanNotFoundException(Long id) {
        super("Could not find Plan " + id);
    }
}