package dynama.reinigung.api.exceptions;

public class BidNotFoundException extends RuntimeException {

    public BidNotFoundException(Long id) {
        super("Could not find Bid " + id);
    }
}