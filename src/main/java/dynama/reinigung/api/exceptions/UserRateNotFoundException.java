package dynama.reinigung.api.exceptions;

public class UserRateNotFoundException extends RuntimeException {

    public UserRateNotFoundException(Long id) {
        super("Could not find UserRate " + id);
    }
}