package dynama.reinigung.api.exceptions;

public class PlanGoalNotFoundException extends RuntimeException {

    public PlanGoalNotFoundException(Long id) {
        super("Could not find Plan Goal " + id);
    }
}